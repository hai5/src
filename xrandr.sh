#!/usr/bin/env bash
# @author: Nguyen Cong Hai
# @created: 2015-10-02
# @version: 1.2.1
# License: please see the attached LICENSE file
#
# This is my wrapper around xrandr


initialOnlyScreen='' # non-null if initially there is only 1 screen active

MARKER_FIFO="/tmp/nch_shade.fifo"

_quitErr_ () {
    # perform some steps prior to quit with error:

    # if --verbatim: don't print out the prefix "$0: " before the message:
    case "$1" in
        --verbatim) shift 1 ;;
        *) printf "$0: " >&2 ;;
    esac

    # print the main message:
    echo -e "$1\n" >&2

    # handle exit code:
    case "$#" in
        1) exit 1 ;;
        2) exit "$2" ;;
        *) echo "$0->${BASH_SOURCE}: wrong number of args" >&2 ; exit 2 ;;
    esac

} # _quitErr_

shadeScreen() {
    case "$1" in
        toggle-shade)    # toggling shade/unshade:

            if [ -e "${MARKER_FIFO}" ] ; then
                shadeScreen "unshade"
            else
                shadeScreen "shade"
            fi

            ;;

        shade)
            xcalib -red 1.5 1 30 -green 1.5 1 40 -blue  1.5 1 60 -alter \
                && mkfifo "${MARKER_FIFO}" # the MARKER_FIFO is to let system know it is being 'shaded'

            ;;

        dim) xcalib -co 75 -a

             ;;

        clear-shade|unshade)
            xcalib -clear && if [ -e "${MARKER_FIFO}" ] ; then rm "${MARKER_FIFO}" ; fi

            ;;
    esac
}

parseDisplay () {
    ## args: null string | space-separated display names
    ## dependencies: initialOnlyScreen initialActiveScreens
    if (echo "$*" | grep -Eq '^[[:space:]]*$') ; then # if args are blank/spaces only
        [ -z "$initialOnlyScreen" ] && _quitErr_ "multiple screens active -> please specify display names to operate on"
        echo "$initialOnlyScreen"
    else
        for eachDisplay; do
            local screenShortname="$eachDisplay"
            [ -z "$screenShortname" ] && _quitErr_ "expecting operand: VGA/LVDS/... etc"

            local selectedScreen="$(echo "${initialActiveScreens}" | grep -F "$screenShortname")"

            [ -z "$screenShortname" ] && _quitErr_ "cannot find display $screenShortname among detected active displays:\n${initialActiveScreensNRes}\n"

            echo "$eachDisplay"

        done
    fi
}

printHelp () {
    sed -n '/\<case\>/,/\<esac\>/p' "${BASH_SOURCE}" | sed -n "/^[ \t]\+[^()]\+)/p"
}


case "$1" in
    --debug) set -x ; shift 1 ;;
esac

if [ $# -eq 0 ] ; then
    printHelp
    exit
fi

## all-screens ops:
case "$1" in
    toggle-shade|shade|dim|clear-shade|unshade) shadeScreen "$1" ; shift 1 ;;
esac ; immRetval=$?

[ $# -eq 0 ] && exit $immRetval  # exit with latest return code if there's nothing else to be done

## ops on connected AND activated (has * in xrandr) output:

initialArgs="$*" # used this to avoid being overshadowed inside the do-while loop and against being destroyed when I call eval set -- ... somewhere else

initialActiveScreensNRes="$( \
xrandr -q \
  | awk '/[^s]connected|\*/ ' \
  | sed 's/^\([^\ \t]\+\)[\ \t]\+connected.*$/\1/g' \
  | awk ' {$1=$1};1 ' \
  | awk ' /\*/ { print x " " $0}; { x=$0 } ' \
)" # output will be like VGA1 1024x768 75.08 75.03* 60.00

initialActiveScreens="$(printf "%s" "${initialActiveScreensNRes}" | awk '{print $1}')"

if [ $(echo "${initialActiveScreens}" | wc -l) -eq 1 ] ; then
    initialOnlyScreen="$initialActiveScreens"
else
    initialOnlyScreen=''
fi

[ -z "$initialActiveScreensNRes" ] && _quitErr_ "ERROR:no active output found. Exit now.\n"

## parse the flags into seperate operations (UNCHAINing the flag into individual lines):
finalcmd='xrandr'
while read -r line ; do {
    eval set -- "$line"
    [ $# -eq 0 ] && break
    args="$1" ; shift 1

    case "$args" in
        ## DONT CHANGE for EACH CASE:
        ### PREFIX -: deliberately use to facilitate parsing of token

        -lsr|--list-active-screens-and-res) echo "${initialActiveScreensNRes}" ; exit ;; # list active screen and their associated resolutions
        -ls|--list-active-screens) echo "${initialActiveScreens}" ; exit ;; # list active screen without their associated resolution

	--off-temp) # temporarily turn off the display. press enter to turn it on
	    finalcmd+=" --output LVDS1 --off; read;  xrandr --output LVDS1 --mode 1366x768"
	;;
        ## DONT CHANGE -off into --off: used to differentiate this from stock xrandr flag
        -off|--off) # [displayname|...] # turn off a display (display is the exact name of the display, e.g. VGA-1, VGA1, HDMI...
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):
            [ -n "$initialOnlyScreen" ] && _quitErr_ "This is the only screen active. \
To turn this off (i.e. there is no screen on), \
please call the command\n \
\t'command -p xrandr --output $initialOnlyScreen --off"

            for i in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output ${i} --off"
            done
            ;;
	--which-brand) # must be preceded with --output
	    echo "IMPLEMENTATION STARTED, NOT YET COMPLETED" >&2
	    exit 1
	    xrandr --properties | grep EDID:
	    ;;
        -bri|--brightness) # <brightness-percentage> [displayname1|...]
            ## ALSO SEE other methods for this at end of this code
            ## DONT ARGS ORDER: flexible args at the end - don't change this order
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):

            bri="$1" ; shift 1
            for i in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output $i --brightness $bri"
            done

            ;;
        -res|--resolution)
	    # <1024|1150|1152|1280|...> <display|blank if only one display> # switch to another resoulution
            ## DONT ARGS ORDER: flexible args at the end - don't change this order
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):
            ## parse resolution
            res="$1" ; shift 1
            for eachScreen in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output $eachScreen --mode $res"
            done

            ;;
	--only-one-display)
	    #* turning off laptop screen if VGA screen active:
	    #** put into subshell to localize the variable:	    
	    connectedScreen="$(xrandr | grep -E '[[:space:]]+connected[[:space:]]' | awk '{print $1}')"
	    if [ $(wc -l <<< "${connectedScreen}" ) -ge 2 ] ; then
		finalcmd="xrandr --output "$(head -1 <<< "${connectedScreen}")" --off"
	    fi	    
	    ;;
        -m|--install-mode) # <output (eg VGA1)> <mode-width eg 1024> <mode-height-eg 768> <mode-rate eg 75.0>
	    if [ $# -ne 4 ] ; then
		printf " [ERROR]:${BASH_SOURCE}: expecting exactly 4 args, got $#:\n" >&2
		echo " [ERROR]:" ${@} >&2
		exit 1
	    fi
	    
	    readonly _output_="$1" ; shift 1
	    readonly _width_="$1" ; shift 1
	    readonly _height_="$1" ; shift 1
	    readonly _rate_="$1" ; shift 1

	    # MUST set after _width_ and _height_ etc
	    readonly _mymode_="my${_width_}x${_height_}x${_rate_}" # the starting char is letter instead of number to be sure the mode name is valid
	    # =================================
	    # method 3 (WORKING slackware 14.2)
	    # =================================
	    # check if the modename already exist
	    readonly _mode_exist_="$(xrandr | fgrep "$_mymode_")"
	    if [ -n "$_mode_exist_" ] ; then
		printf " [ERROR]:${BASH_SOURCE}: mode already exist\n  ${_mode_exist_} \n" >&2
		exit 1
	    fi
	    
	    set -e
	    xrandr --newmode "$_mymode_" $(cvt $_width_ $_height_ $_rate_  | egrep 'Modeline' | sed --expression 's/^.*Modeline[[:space:]]\+[^[:space:]]\+[[:space:]]\+\(.*\)$/\1/')
	    xrandr --addmode $_output_ "$_mymode_"

	    xrandr --output $_output_ --mode $_mymode_
	    
	    # =================================
	    # method 2 (PRINT INSTRUCTION ONLY)
	    # =================================
	    # 	    printf "
	    # Run cvt <modewidth> <modeheight> <targetrate>
	    # copy the line starts with Modeline, but exclude the 'Modeline' and the word right after it (the word right after it is the arbitrary name of the mode)
	    # then xrandr --newmode <any name for your mode> <the copied mode line>
	    # then xrandr --addmode <any name for your mode>
	    # voila!
	    # "
	    # 	    exit
	    
	    # =================================
	    # method 1 (FAILED)
	    # =================================

	    # | awk -v FS='Modeline' ' { print $2 } '
	    # 	    newmodeline="$(cvt $modewidth $modeheight $targetrate \
		# 	    | grep 'Modeline' \
		# 	    | awk '{print $2}' \
		# 	    | tr -d '\"' \
		# )"

	    # 	    if [ -z "$newmodeline" ] ; then
	    # 		_quitErr_ " [ERROR] failed creating new mode line via cvt"
	    # 	    fi
	    
	    # 	    newresolution="$(echo ${newmodeline} | awk ' { print $1 } ' | sed -e 's/^[\t\n\ ]\+//g; s/[\t\n\ ]\+$//g' )"

	    # 	    ## SYNTAX: don't know why some unseen new line char get into the command -> xrandr failed to parse the whole string:
	    # 	    ##            eval "xrandr | fgrep -q '${newresolution}'"
	    # 	    ##            if [ $? -ne 0 ] ; then {
	    # 	    xrandr --newmode ${newmodeline}

	    # 	    ## check if mode has already been added to selected output:
	    # 	    ## DONT DELETE eval: without eval it won't work ??? (due to nested case!? sh behave like vim does!?)
	    # 	    ## SYNTAX: don't know why some unseen new line char get into the command -> xrandr failed to parse the whole string without tr -d:
	    # 	    eval $(echo "xrandr --addmode ${selectedoutputname} ${newresolution}" | tr -d '\n')

	    # 	    ## SYNTAX: without eval, it won't work (because of nested case maybe!?)
	    # 	    eval $(echo "xrandr --output ${selectedoutputname} --mode ${newresolution} --rate ${targetrate}" | tr -d '\n')

	    ;;
	-h|--help) printHelp ; exit ;;

	*) finalcmd+=" ${initialArgs}" ; break ;;

    esac
} ; done < <(echo "$*" | awk 'BEGIN { RS="[ \t]+[-]{1,2}"} {print}')
echo -e "Command to be executed:\n\t${finalcmd}" >&2
eval "${finalcmd}"
exit
#
# ## DONT DELETE: don't delete this line of code - it is hard to come up with:
# ##  selectedoutputrate="$(echo "${initialActiveScreensNRes}" |  awk -v RS='[\ \t]+' ' { print } ' | awk '/^[0-9\.]+[*]/' | sed -e 's/[*+]//g')"
#
#
#  ## setting brightness <<<
#  ## METHOD 2: <<<2
#  ## palacsint "http://askubuntu.com/questions/149054/how-to-change-lcd-brightness-from-command-line-or-via-script" accessed 2015-11-10
#  ## max brightness number:
#  ##cat /sys/class/backlight/intel_backlight/max_brightness
#  ## min brightness number:
#  ##cat /sys/class/backlight/intel_backlight/bl_power
#  ##cat /sys/class/backlight/intel_backlight/actual_brightness
#  ## change:
#  ##echo 400 > /sys/class/backlight/intel_backlight/brightness
#
#
#  ## METHOD 3:
#  ## Raja "http://askubuntu.com/questions/149054/how-to-change-lcd-brightness-from-command-line-or-via-script" accessed 2015-11-10
#  ##sudo apt-get install xbacklight
#  ##xbacklight -set 50
#  ##xbacklight -inc 10
#  ##xbacklight -dec 10
#  ##
