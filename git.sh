#!/usr/bin/env bash
# Author: Cong Hai Nguyen
# License: please see the LICENSE file attached
## created: 2016-08-01

# Version: 0.1.0

# * Name
#
# This is my wrapper around git common tasks.
#

# * Synopsis
#
# git.sh <flags> [args]
#
# * Description
# 
# Please see git.sh --help for the syntax.
#

set -u

# * Internal variables:

# # exit codes when program exit with error

_ERR_SYNTAX=3
_ERR_USER=4
_ERR_FILENOTFOUND=5
_ERR_FILEEXIST=6
_ERR_OTHER=7

# 

# # if argcount not sufficient, exit with $_ERR_SYNTAX defined in
# lib-errnum_.sh
#
# User needs to submit argcount because this code could be put inside
# a function, whose $# (argcount) will shadow the argcount of the
# command-line args

_exit_argcount_low () {
    if (test $1 -lt $2) ; then
	echo "FATAL: Syntax error: not enough arguments (expected $2, got $1)" >&2
	exit $_ERR_SYNTAX
    fi
}


_exit_argcount_low $# 1


# * functions

_merge_to_master () {
    ## Source: https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging
    #
    ## if there is conflict (i.e. 2 different changes to same part
    ## from different branches) , Git won’t let you switch
    ## branches -> workaround: stashing and commit amending

    git checkout  master
    ## switch to master

    git merge $*
    ## merge local branch into master
}

_current_branch_name () {
    git branch | grep -E '^\*' | sed --expression 's/^\*[[:space:]]\+//'
}

_branch_create_and_switch () {
    git branch $* && git checkout $*
}


opt="${1}" ; shift 1

case "${opt}" in
    
    ### restore a file to its commit number
    --restore-version) # <commit-num> <file-name>
	git checkout $*
	;;
    
    ### edit the commit message
    --edit-commit-message)
	git commit --amend
	;;

    ### list files in a commit
    --list-committed) # <commit-hash-id>
	if [ $# -eq 0 ] ; then
	    set -- "HEAD"
	fi
	git diff-tree --no-commit-id --name-only -r "$@"
	;;

    --list-staged)
	git diff --cached --name-status
	;;
    
    ### pretty print 1 liner for log
    --view-log)
	if [ $# -eq 0 ] ; then
	    git log --pretty=oneline
	else
	    git log --pretty=oneliner -- "$@"
	fi
	;;
    
    ### list files being staged for commit
    --list-added)
	git diff --cached --name-status
	;;
    
    ### list files being tracked in this repo
    --list-committed)
	git ls-tree -r master --name-only
	;;

    --diff-last) # diff current!? file with its most recent!? commit
	git diff "$1"~ "$1"
	;;
    
    ### adding remote repo from https://github.com/
    --add-remote-repo) # <github-username>/<project-name>.git
	git remote add origin https://github.com/$*
	;;

    ### pushing the thing to remote repo (ie github.com if you have
    ### define the remote one as such)
    --push-to-remote) # no args
	git commit -a && git push -u origin HEAD:master ;;
    
    ### create a local branch <branch-name>
    --make-branch-and-switch) # <branch-name>
	_branch_create_and_switch $*
	;;

    ### switch to a branch <branch-name>
    --switch-branch) # <branch-name>
	git checkout $*
	;;

    ### merge to local master and preserve all logs in the branch
    --merge-to-master) # <branch-name>
	_merge_to_master $*
	;;

    ### merge current branch into master + reset current branch to blank slate
    --merge-to-master-and-die)
	cur_branch_name="$(_current_branch_name)"
	
	if [ "$cur_branch_name" == "master" ] ; then
	    echo " WARNING: Already in 'master' branch" >&2
	    exit 1
	fi

	set -e
	
	_merge_to_master "$cur_branch_name"

	git branch -d "$cur_branch_name"

	;;
    
    --merge-to-remote-master) # <branch-name>
	# steps to do (sequentially):
	set -e
	readonly branch_name=$1 ; shift 1
	git checkout master
	git pull origin master
	echo "you are about to resolve conflict and all:"
	git merge branch_name
	git push origin masters
	;;

    --get-url-remote)
	git config --get remote.origin.url
	;;
    
    --delete-branch) # <branch-name>
	git branch -d $*
	;;

    ### merge the last CONSECUTIVE commits
    --merge-last-commits) #<number of the last CONSECUTIVE commits>
	git reset --soft HEAD~"$1"
	git commit --amend
	;;
    
    ### merge any 2 commits
    ## reference: http://stackoverflow.com/questions/2563632/how-can-i-merge-two-commits-into-one accessed 2017-01-09. archived: ~/doc/git./merge/how-can-i-merge-two-commits-into-one accessed 2017-01-09
    --merge-any-2-commits) #<once in the commit editor, select p(pick) <old_commit> and s(squash) <newer commit>
	git rebase --interactive HEAD~2
	;;
    
    ## attempt to fix merging conflict (i.e. 2 different changes to same part
    ## from different branches)
    --fix-conflict) # <no args>
	printf "steps to do (sequentially):
git status
git mergetool [used to manually edit the conflict]
git status [chec the repo after the fix]
git commit [if you are happy with the status]
"
	;;

    # 

    *) git "$opt" $*
       exit $?
       ;;

esac
